# MiuiCamera - from laurel_sprout V11.0.23.0.QFQMIXM - manually patched
-priv-app/MiuiCamera/MiuiCamera.apk;OVERRIDES=Camera2,GoogleCameraGo;platform

# MiuiCamera - libraries - from laurel_sprout V12.0.3.0.RFQMIXM
lib64/libcamera_algoup_jni.xiaomi.so:priv-app/MiuiCamera/lib/arm64/libcamera_algoup_jni.xiaomi.so
lib64/libcamera_mianode_jni.xiaomi.so:priv-app/MiuiCamera/lib/arm64/libcamera_mianode_jni.xiaomi.so
lib64/libcamera_jpegutil_jni.xiaomi.so:priv-app/MiuiCamera/lib/arm64/libcamera_jpegutil_jni.xiaomi.so
lib64/libclone_c++_shared.so:priv-app/MiuiCamera/lib/arm64/libclone_c++_shared.so
lib64/libcom.xiaomi.camera.requestutil.so:priv-app/MiuiCamera/lib/arm64/libcom.xiaomi.camera.requestutil.so
lib64/libDocumentProcess.so:priv-app/MiuiCamera/lib/arm64/libDocumentProcess.so
lib64/libDollyZoom.so:priv-app/MiuiCamera/lib/arm64/libDollyZoom.so
lib64/libfenshen_snpe.so:priv-app/MiuiCamera/lib/arm64/libfenshen_snpe.so
lib64/libffmpeg.so:priv-app/MiuiCamera/lib/arm64/libffmpeg.so
lib64/libinception_video.so:priv-app/MiuiCamera/lib/arm64/libinception_video.so
lib64/libmimoji_avatarengine.so:priv-app/MiuiCamera/lib/arm64/libmimoji_avatarengine.so
lib64/libmimoji_bokeh_845_video.so:priv-app/MiuiCamera/lib/arm64/libmimoji_bokeh_845_video.so
lib64/libmimoji_jni.so:priv-app/MiuiCamera/lib/arm64/libmimoji_jni.so
lib64/libmimoji_soundsupport.so:priv-app/MiuiCamera/lib/arm64/libmimoji_soundsupport.so
lib64/libmimoji_tracking.so:priv-app/MiuiCamera/lib/arm64/libmimoji_tracking.so
lib64/libmimoji_video2gif.so:priv-app/MiuiCamera/lib/arm64/libmimoji_video2gif.so
lib64/librecord_video.so:priv-app/MiuiCamera/lib/arm64/librecord_video.so
lib64/libvvc++_shared.so:priv-app/MiuiCamera/lib/arm64/libvvc++_shared.so


# Permission
etc/default-permissions/miuicamera-permissions.xml
etc/permissions/privapp-permissions-miuicamera.xml

# Fonts
vendor/camera/fonts/MFYueYuan-Regular.ttf
vendor/camera/fonts/MI+LanTing_GB+Outside+YS_V2.3_20160322.ttf
vendor/camera/fonts/MIUI_Time.ttf
vendor/camera/fonts/FZMiaoWuJW.ttf

# ArcSoft config
vendor/etc/camera/beauty_ui9_intelligent_params.config
